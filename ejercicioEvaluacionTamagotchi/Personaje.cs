﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicioEvaluacionTamagotchi
{
    public class Personaje
    {
        //poner en private y dar permisos
        private string _vida = " ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄";
        public string Vida
        {
            get
            {
                return this._vida;
            }
            set
            {
                throw new ArgumentException("La vida maxima son 10 corazones");
            }
        }
        private string _vidaD = " ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀";
        public string VidaD
        {
            get
            {
                return this._vidaD;
            }
            set
            {
                throw new ArgumentException("La vida maxima son 10 corazones");
            }
        }
        private string _vidaT = "   ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀  ";
        public string VidaT
        {
            get
            {
                return this._vidaT;
            }
            set
            {
                throw new ArgumentException("La vida maxima son 10 corazones");
            }
        }
        public int menu;

        //accion correr
        public string corre()
        {
            //ponemos un if para saber si falta una vida y asi solo le resta la que le falta.
            if (this._vida.Length == 8) {//borramos el ESPACIO Y EL VALOR del string
                this._vida = this._vida.Remove(this._vida.Length - 8);
            }
            else
            {
                this._vida = this.Vida.Remove(this._vida.Length - 16);
            }
            return this._vida;
        }
        public string correD()
        {
            if (this._vidaD.Length == 8)
            {//borramos el ESPACIO Y EL VALOR del string
                this._vidaD = this._vidaD.Remove(this._vidaD.Length - 8);
            }
            else
            {
                this._vidaD = this._vidaD.Remove(this._vidaD.Length - 16);
            }
            return this._vidaD;
        }
        public string correT()
        {
            if (this._vidaT.Length == 8)
            {//borramos el ESPACIO Y EL VALOR del string
                this._vidaT = this._vidaT.Remove(this._vidaT.Length - 8);
            }
            else
            {
                this._vidaT = this._vidaT.Remove(this._vidaT.Length - 16);

            }
            return this._vidaT;
        }
        //duerme
        public string duerme()
        {
            if (this._vida.Length != 72) {
                this._vida += " ▄██▄██▄ ▄██▄██▄";
                return this._vida;
            }
            else
            {
                this._vida += " ▄██▄██▄";
                return this._vida;
            }
        }
        public string duermeD()
        {
            if (this._vidaD.Length != 72)
            {
                this._vidaD += " ▀█████▀ ▀█████▀";
                return this._vidaD;
            }
            else
            {
                this._vidaD += " ▀█████▀";
                return this._vidaD;
            }
        }
        public string duermeT()
        {
            if (this._vidaT.Length != 72) {
                this._vidaT += "   ▀█▀     ▀█▀  ";
                return this._vidaT;
            }
            else
            {
                this._vidaT += "   ▀█▀  ";
                return this._vidaT;
            }
            
        }

        //accion discutir
        public string discute()
        {
            //borramos el ESPACIO Y EL VALOR del string
            this._vida = this._vida.Remove(this._vida.Length - 8);
            return this._vida;
        }
        public string discuteD()
        {
            //borramos el ESPACIO Y EL VALOR del string
            this._vidaD = this._vidaD.Remove(this._vidaD.Length - 8);
            return this.VidaD;
        }
        public string discuteT()
        {
            //borramos el ESPACIO Y EL VALOR del string
            this._vidaT = this._vidaT.Remove(this._vidaT.Length - 8);
            return this._vidaT;
        }
        //canta
        public string canta()
        {
            this._vida += " ▄██▄██▄";
            return this._vida;
        }
        public string cantaD()
        {
            this._vidaD += " ▀█████▀";
            return this._vidaD;
        }
        public string cantaT()
        {
            this._vidaT += "   ▀█▀  ";
            return this._vidaT;
        }
        //Baila
        public string baila()
        {
            this._vida += " ▄██▄██▄";
            return this._vida;
        }
        public string bailaD()
        {
            this._vidaD += " ▀█████▀";
            return this._vidaD;
        }
        public string bailaT()
        {
            this._vidaT += "   ▀█▀  ";
            return this._vidaT;
        }



    }
}

    
