﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicioEvaluacionTamagotchi
{
    class Program
    {
        static void Main(string[] args)
        {
            Personaje tamagotchi = new Personaje();
            
            //SI MODIFICO LOS CORAZONES EL PROGRAMA PETA... AQUI LA PUERBA:
            /*tamagotchi.Vida =  " ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄ ▄██▄██▄    sdfshhh";
            tamagotchi.VidaD =   " ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀ ▀█████▀     fsdfasd";
            tamagotchi.VidaT =   "   ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀     ▀█▀       sfasd";*/

            Program.escribirMenu();
            //Hacer que cuando el string este vacio ponga que el juego ha terminado porque ha muerto el personaje

            while (tamagotchi.Vida != "") {

                //Controlamos el fallo por si el valor introducido es null
                try
                {
                    tamagotchi.menu = int.Parse(Console.ReadLine());
                }
                catch
                {
                    //va ir a la excepcion para que no falle "default"
                }
                Console.Clear();
                Program.escribirMenu();

                switch (tamagotchi.menu)
                {

                    case 1:
                        Console.WriteLine("\t \tLa vida de tu personaje ha bajado");
                        Console.WriteLine("\n");
                        Console.WriteLine("\t \t" + tamagotchi.corre());
                        Console.WriteLine("\t \t" + tamagotchi.correD());
                        Console.WriteLine("\t \t" + tamagotchi.correT());
                        Console.WriteLine("\n");
                        Console.WriteLine("\t \tTu personaje esta agotado por correr 10km");
                        Console.WriteLine("\n");
                        Program.correCansado();
                        break;

                    case 2:
                        if (tamagotchi.Vida.Length == 80)
                        {
                            Console.WriteLine("\t\tVida al maximo.");
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \t" + tamagotchi.Vida);
                            Console.WriteLine("\t \t" + tamagotchi.VidaD);
                            Console.WriteLine("\t \t" + tamagotchi.VidaT);
                            Console.WriteLine("\n");
                            Console.WriteLine("\t\tTu personaje ha descansado bien");
                            Console.WriteLine("\n");
                            Program.duermeDescansado();
                            break;
                        }
                        else
                        {
                            Console.WriteLine("\t \tTu personaje sube de vida");
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \t" + tamagotchi.duerme());
                            Console.WriteLine("\t \t" + tamagotchi.duermeD());
                            Console.WriteLine("\t \t" + tamagotchi.duermeT());
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \tTu personaje ha descansado bien");
                            Console.WriteLine("\n");
                            Program.duermeDescansado();
                            break;
                        }


                    case 3:
                        Console.WriteLine("\t \tLa vida de tu personaje ha bajado");
                        Console.WriteLine("\n");
                        Console.WriteLine("\t \t" + tamagotchi.discute());
                        Console.WriteLine("\t \t" + tamagotchi.discuteD());
                        Console.WriteLine("\t \t" + tamagotchi.discuteT());
                        Console.WriteLine("\n");
                        Console.WriteLine("\t \tTu personaje esta triston");
                        Console.WriteLine("\n");
                        discuteTriste();
                        break;

                    
                    case 4:
                        if (tamagotchi.Vida.Length == 80)
                        {
                            Console.WriteLine("\t \tVida al maximo.");
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \t" + tamagotchi.Vida);
                            Console.WriteLine("\t \t" + tamagotchi.VidaD);
                            Console.WriteLine("\t \t" + tamagotchi.VidaT);
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \tTu personaje sigue cantando. Le gusta mucho cantar!!");
                            Console.WriteLine("\n");
                            Program.cantarContento();
                            
                            break;
                        }
                        else
                        {
                            Console.WriteLine("\t \tLa vida de tu personaje ha subido");
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \t" + tamagotchi.canta());
                            Console.WriteLine("\t \t" + tamagotchi.cantaD());
                            Console.WriteLine("\t \t" + tamagotchi.cantaT());
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \tTu personaje sigue cantando. Le gusta mucho cantar!!");
                            Console.WriteLine("\n");
                            Program.cantarContento();

                            break;
                        }
                    case 5:
                        if (tamagotchi.Vida.Length == 80)
                        {
                            Console.WriteLine("\t \tVida al maximo");
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \t" + tamagotchi.Vida);
                            Console.WriteLine("\t \t" + tamagotchi.VidaD);
                            Console.WriteLine("\t \t" + tamagotchi.VidaT);
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \tTu personaje esta bailando");
                            Console.WriteLine("\n");
                            Program.bailarContento();

                            break;
                        }
                        else
                        {
                            Console.WriteLine("\t \tLa vida de tu personaje ha subido");
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \t" + tamagotchi.baila());
                            Console.WriteLine("\t \t" + tamagotchi.bailaD());
                            Console.WriteLine("\t \t" + tamagotchi.bailaT());
                            Console.WriteLine("\n");
                            Console.WriteLine("\t \tTu personaje esta bailando");
                            Console.WriteLine("\n");
                            Program.bailarContento();

                            break;
                        }
                    default:
                        Console.WriteLine("\t \tVida al maximo");
                        Console.WriteLine("\n");
                        Console.WriteLine("\t \t" + tamagotchi.Vida);
                        Console.WriteLine("\t \t" + tamagotchi.VidaD);
                        Console.WriteLine("\t \t" + tamagotchi.VidaT);
                        Console.WriteLine("\n");
                        Console.WriteLine("\t \tINTRODUZCA UN VALOR VALIDO");
                        break;
                }

                }


            Console.Clear();
            Program.gameOver();
            Console.ReadKey();
            



        }

        
        public static void escribirMenu()
        {
            Console.WriteLine(@" _  _   ___                        ___  _  ___                                 ____ _  ___   _                _        ");
            Console.WriteLine(@"/ |(_) / __| ___  _ _  _ _  ___   |_  )(_)|   \  _  _  ___  _ _  _ __   ___   |__ /(_)|   \ (_) ___ __  _  _ | |_  ___ ");
            Console.WriteLine(@"| | _ | (__ / _ \| '_|| '_|/ -_)   / /  _ | |) || || |/ -_)| '_|| '  \ / -_)   |_ \ _ | |) || |(_-</ _|| || ||  _|/ -_)");
            Console.WriteLine(@"|_|(_) \___|\___/|_|  |_|  \___|  /___|(_)|___/  \_,_|\___||_|  |_|_|_|\___|  |___/(_)|___/ |_|/__/\__| \_,_| \__|\___|");
            Console.WriteLine(@"                                                                                                                                           ");
            Console.WriteLine(@" _ _   _   ___             _            ___  _  ___        _  _       ");
            Console.WriteLine(@"| | | (_) / __| __ _  _ _ | |_  __ _   | __|(_)| _ ) __ _ (_)| | __ _ ");
            Console.WriteLine(@"|_  _| _ | (__ / _` || ' \|  _|/ _` |  |__ \ _ | _ \/ _` || || |/ _` |");
            Console.WriteLine(@"   |_|(_) \___|\__,_||_||_|\__|\__,_|  |___/(_)|___/\__,_||_||_|\__,_|");
            Console.WriteLine(@"                                                                                                                                           ");
        }
        public static void correCansado()
        {
            Console.WriteLine("\t\t\t███████▄▄███████████▄");
            Console.WriteLine("\t\t\t▓▓▓▓▓▓█░░░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t▓▓▓▓▓▓█░░░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t▓▓▓▓▓▓█░░░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t▓▓▓▓▓▓█░░░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t▓▓▓▓▓▓█░░░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t▓▓▓▓▓▓███░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t██████▀░░█░░░░██████▀");
            Console.WriteLine("\t\t\t░░░░░░░░░█░░░░█");
            Console.WriteLine("\t\t\t░░░░░░░░░░█░░░█");
            Console.WriteLine("\t\t\t░░░░░░░░░░░█░░█");
            Console.WriteLine("\t\t\t░░░░░░░░░░░█░░█");
            Console.WriteLine("\t\t\t░░░░░░░░░░░░▀▀");
        }
        public static void bailarContento()
        {
            Console.WriteLine("\t\t\t░░░░░░░░░░░░░░░░░░░░░░█████████");
            Console.WriteLine("\t\t\t░░███████░░░░░░░░░░███▒▒▒▒▒▒▒▒███");
            Console.WriteLine("\t\t\t░░█▒▒▒▒▒▒█░░░░░░░███▒▒▒▒▒▒▒▒▒▒▒▒▒███");
            Console.WriteLine("\t\t\t░░░█▒▒▒▒▒▒█░░░░██▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t░░░░█▒▒▒▒▒█░░░██▒▒▒▒▒██▒▒▒▒▒▒██▒▒▒▒▒███");
            Console.WriteLine("\t\t\t░░░░░█▒▒▒█░░░█▒▒▒▒▒▒████▒▒▒▒████▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t░░░█████████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t░░░█▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t░██▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒██▒▒▒▒▒▒▒▒▒▒██▒▒▒▒██");
            Console.WriteLine("\t\t\t██▒▒▒███████████▒▒▒▒▒██▒▒▒▒▒▒▒▒██▒▒▒▒▒██");
            Console.WriteLine("\t\t\t█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒████████▒▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t██▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t░█▒▒▒███████████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒██");
            Console.WriteLine("\t\t\t░██▒▒▒▒▒▒▒▒▒▒████▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█");
            Console.WriteLine("\t\t\t░░████████████░░░█████████████████");
        }
        public static void discuteTriste()
        {
            Console.WriteLine("\t\t\t───────▄▀▀▀▀▀▀▀▀▀▀▄▄");
            Console.WriteLine("\t\t\t────▄▀▀░░░░░░░░░░░░░▀▄");
            Console.WriteLine("\t\t\t──▄▀░░░░░░░░░░░░░░░░░░▀▄");
            Console.WriteLine("\t\t\t──█░░░░░░░░░░░░░░░░░░░░░▀");
            Console.WriteLine("\t\t\t─▐▌░░░░░░░░▄▄▄▄▄▄▄░░░░░░░▐");
            Console.WriteLine("\t\t\t─█░░░░░░░░░░░▄▄▄▄░░▀▀▀▀▀░░█");
            Console.WriteLine("\t\t\t▐▌░░░░░░░▀▀▀▀░░░░░▀▀▀▀▀░░░▐▌");
            Console.WriteLine("\t\t\t█░░░░░░░░░▄▄▀▀▀▀▀░░░░▀▀▀▀▄░█");
            Console.WriteLine("\t\t\t█░░░░░░░░░░░░░░░░▀░░░▐░░░░░▐▌");
            Console.WriteLine("\t\t\t▐▌░░░░░░░░░▐▀▀██▄░░░░░░▄▄▄░▐▌");
            Console.WriteLine("\t\t\t─█░░░░░░░░░░░▀▀▀░░░░░░▀▀██░░█");
            Console.WriteLine("\t\t\t─▐▌░░░░▄░░░░░░░░░░░░░▌░░░░░░█");
            Console.WriteLine("\t\t\t──▐▌░░▐░░░░░░░░░░░░░░▀▄░░░░░█");
            Console.WriteLine("\t\t\t───█░░░▌░░░░░░░░▐▀░░░░▄▀░░░▐▌");
            Console.WriteLine("\t\t\t───▐▌░░▀▄░░░░░░░░▀░▀░▀▀░░░▄▀");
            Console.WriteLine("\t\t\t───▐▌░░▐▀▄░░░░░░░░░░░░░░░░█");
            Console.WriteLine("\t\t\t───▐▌░░░▌░▀▄░░░░▀▀▀▀▀▀░░░█");
            Console.WriteLine("\t\t\t───█░░░▀░░░░▀▄░░░░░░░░░░▄▀");
            Console.WriteLine("\t\t\t──▐▌░░░░░░░░░░▀▄░░░░░░▄▀");
            Console.WriteLine("\t\t\t─▄▀░░░▄▀░░░░░░░░▀▀▀▀█▀");
            Console.WriteLine("\t\t\t▀░░░▄▀░░░░░░░░░░▀░░░▀▀▀▀▄▄▄▄▄");
        }
        public static void duermeDescansado()
        {
            Console.WriteLine("\t\t\t░░█▀░░░░░░░░░░░▀▀███████░░░░");
            Console.WriteLine("\t\t\t░░█▌░░░░░░░░░░░░░░░▀██████░░░");
            Console.WriteLine("\t\t\t░█▌░░░░░░░░░░░░░░░░███████▌░░");
            Console.WriteLine("\t\t\t░█░░░░░░░░░░░░░░░░░████████░░");
            Console.WriteLine("\t\t\t▐▌░░░░░░░░░░░░░░░░░▀██████▌░░");
            Console.WriteLine("\t\t\t░▌▄███▌░░░░▀████▄░░░░▀████▌░░");
            Console.WriteLine("\t\t\t▐▀▀▄█▄░▌░░░▄██▄▄▄▀░░░░████▄▄░");
            Console.WriteLine("\t\t\t▐░▀░░═▐░░░░░░══░░▀░░░░▐▀░▄▀▌▌");
            Console.WriteLine("\t\t\t▐░░░░░▌░░░░░░░░░░░░░░░▀░▀░░▌▌");
            Console.WriteLine("\t\t\t▐░░░▄▀░░░▀░▌░░░░░░░░░░░░▌█░▌▌");
            Console.WriteLine("\t\t\t░▌░░▀▀▄▄▀▀▄▌▌░░░░░░░░░░▐░▀▐▐░");
            Console.WriteLine("\t\t\t░▌░░▌░▄▄▄▄░░░▌░░░░░░░░▐░░▀▐░░");
            Console.WriteLine("\t\t\t░█░▐▄██████▄░▐░░░░░░░░█▀▄▄▀░░");
            Console.WriteLine("\t\t\t░▐░▌▌░░░░░░▀▀▄▐░░░░░░█▌░░░░░░");
            Console.WriteLine("\t\t\t░░█░░▄▀▀▀▀▄░▄═╝▄░░░▄▀░▌░░░░░░");
            Console.WriteLine("\t\t\t░░░▀▄░░░░░░░░░▄▀▀░░░░█░░░░░░░");
            Console.WriteLine("\t\t\t░░░▄█▄▄▄▄▄▄▄▀▀░░░░░░░▌▌░░░░░░");
            Console.WriteLine("\t\t\t░░▄▀▌▀▌░░░░░░░░░░░░░▄▀▀▄░░░░░");
            Console.WriteLine("\t\t\t▄▀░░▌░▀▄░░░░░░░░░░▄▀░░▌░▀▄░░░");
            Console.WriteLine("\t\t\t░░░░▌█▄▄▀▄░░░░░░▄▀░░░░▌░░░▌▄▄");
            Console.WriteLine("\t\t\t░░░▄▐██████▄▄░▄▀░░▄▄▄▄▌░░░░▄░");
            Console.WriteLine("\t\t\t░▄▀░██████████████████▌▀▄░░░░");
            Console.WriteLine("\t\t\t▀░░░█████▀▀░░░▀███████░░░▀▄░░");
            Console.WriteLine("\t\t\t░░░░▐█▀░░░▐░░░░░▀████▌░░░░▀▄░");
            Console.WriteLine("\t\t\t░░░░░░▌░░░▐░░░░▐░░▀▀█░░░░░░░▀");
            Console.WriteLine("\t\t\t░░░░░░▐░░░░▌░░░▐░░░░░▌░░░░░░░");
            Console.WriteLine("\t\t\t░╔╗║░╔═╗░═╦═░░░░░╔╗░░╔═╗░╦═╗░");
            Console.WriteLine("\t\t\t░║║║░║░║░░║░░░░░░╠╩╗░╠═╣░║░║░");
            Console.WriteLine("\t\t\t░║╚╝░╚═╝░░║░░░░░░╚═╝░║░║░╩═╝░");
        }
        public static void cantarContento()
        {
            Console.WriteLine("\t\t\t░░░░░░░░░██░█░█░█░█░█░█░█░░░░░");
            Console.WriteLine("\t\t\t░░░░░░░░█░░░░░░░░░░░░░░░░█░░░░░░");
            Console.WriteLine("\t\t\t░░░░░░░█░████████████████░█░░░░░░");
            Console.WriteLine("\t\t\t░░░░░░█░█░░░░░░░░░░░░░░░░█░█░░░░░░░♫░♫");
            Console.WriteLine("\t\t\t░░░░░█░█░░░░░░░░░░░░░░░░░░█░█░░░░░♫░♪░♫");
            Console.WriteLine("\t\t\t░░░░█░█░░░░░░░░░░░░░░░░░░░░█░█░░░░░♪░♫░♪");
            Console.WriteLine("\t\t\t░░░█░█░░░░░░██░░░░░░░░░░░░░░█░█░░░♫░♪░♫");
            Console.WriteLine("\t\t\t░░█░█░░░░░░████░░░░░██░░░░░░░█░█░░░♪░♫░♪");
            Console.WriteLine("\t\t\t░██░█░░░░░░████░░░░████░░░░░░█░██░♫░♪░");
            Console.WriteLine("\t\t\t███░█░░░░░░░██░░░░░░██░░░░░░░█░███ ");
            Console.WriteLine("\t\t\t░██░█░░░░░░░░░░░░░░░░░░░░░░░░█░██░");
            Console.WriteLine("\t\t\t░░█░█░░░░░██░░░░░░░░░░██░░░░░█░█░");
            Console.WriteLine("\t\t\t░░░░░█░░░░░██░░░░░░░░██░░░░░█░░░");
            Console.WriteLine("\t\t\t░░░░░░█░░░░░░█████████░░░░░█░░░");
            Console.WriteLine("\t\t\t░░░░░░░█░░░░░░░░░░░░░░░░░░█░░░");
            Console.WriteLine("\t\t\t░░░░░░░░█░░░░░░░░░░░░░░░░█░░░");
            Console.WriteLine("\t\t\t░░░░░░░░░████████████████░░");

        }

        public static void gameOver()
        {
            Console.WriteLine("\n\n");
            Console.WriteLine("\t\t\t██████╗ █████╗███╗   ██████████╗     ██████╗██╗   ███████████████╗ ");
            Console.WriteLine("\t\t\t██╔════╝██╔══██████╗ ██████╔════╝    ██╔═══████║   ████╔════██╔══██╗");
            Console.WriteLine("\t\t\t██║  ████████████╔████╔███████╗      ██║   ████║   ███████╗ ██████╔╝");
            Console.WriteLine("\t\t\t██║   ████╔══████║╚██╔╝████╔══╝      ██║   ██╚██╗ ██╔██╔══╝ ██╔══██╗");
            Console.WriteLine("\t\t\t╚██████╔██║  ████║ ╚═╝ █████████╗    ╚██████╔╝╚████╔╝█████████║  ██║");
            Console.WriteLine("\t\t\t ╚═════╝╚═╝  ╚═╚═╝     ╚═╚══════╝     ╚═════╝  ╚═══╝ ╚══════╚═╝  ╚═╝");
        }

    }
}
